<?php
// start session 
session_start();
 
function existsInArray($product,$cart){
	foreach ($cart as $comparedProduct) {
		if ($comparedProduct -> id_product == $product -> id_product) {
			return true;
		}
	}
	return false;
}

// get the product id
$id_product = isset($_GET['id']) ? $_GET['id'] : "";
$quantity = isset($_GET['quantity']) ? $_GET['quantity'] : 1;
$page = isset($_GET['page']) ? $_GET['page'] : 1;
 
// make quantity a minimum of 1
$quantity=$quantity<=0 ? 1 : $quantity;
 
// add new item on array
$cart_item=array(
    'quantity'=>$quantity,
    'id_product'=>$id_product
);
 
/*
 * check if the 'cart' session array was created
 * if it is NOT, create the 'cart' session array
 */
if(!isset($_SESSION['cart'])){
    $_SESSION['cart'] = array();
}
 
// check if the item is in the array, if it is, do not add
//if(array_key_exists($id_product, $_SESSION['cart'])){
if(existsInArray($cart_item,$_SESSION['cart'])){
    // redirect to product list and tell the user it was added to cart
    echo $_SESSION['cart'];
    foreach ($_SESSION['cart'] as $comparedProduct) {
		if ($comparedProduct -> id_product == $cart_item -> id_product) {
			$comparedProduct -> quantity = $comparedProduct -> quantity +1;
		}
	}
    //header('Location: products.php?action=exists&id=' . $id_product . '&page=' . $page);
}
 
// else, add the item to the array
else{
    //$_SESSION['cart'][$id_product]=$cart_item;
 
    // redirect to product list and tell the user it was added to cart
    //header('Location: products.php?action=added&page=' . $page);
    array_push($_SESSION['cart'], $cart_item);
    foreach ($_SESSION['cart'] as $product) {
    	echo("Product :".$product);
    	   }
    
}
?>