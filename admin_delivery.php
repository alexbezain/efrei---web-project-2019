<?php
include_once 'CartItemClass.php';
// start session
session_start();
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PMA ADMIN</title>

    <!-- Bootstrap Core CSS -->
    <link href="libs/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="libs/css/small-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
    
    <?php
        include_once 'config.php';

        $id = "";
        $customer_id = "";
        $grand_total = "";
        $status = "";
    ?>

    <?php
    $action = isset($_POST['action']) ? $_POST['action'] : "";
    $id = isset($_POST['id']) ? $_POST['id'] : "";

    switch ($action){
        case "M":
            $record = $link->query("SELECT * FROM orders WHERE id=".$id)->fetch();
            $id = $record['id'];
            $customer_id = $record['customer_id'];
            $grand_total = $record['grand_total'];
            $status = $record['status'];

            break;
        case "S":
            $link->exec("DELETE FROM orders WHERE id =".$id);

            break;
        case "OK":
            $id = isset($_POST['id']) ? $_POST['id'] : "";
            $customer_id = isset($_POST['customer_id']) ? $_POST['customer_id'] : "";
            $grand_total = isset($_POST['grand_total']) ? $_POST['grand_total'] : "";
            $status = isset($_POST['status']) ? $_POST['status'] : "";

            $record = $link->query("SELECT * FROM orders WHERE id = ".$id)->fetch();


            if($id != ""){
                $sql = "";
                if(isset($record['id'])){
                    $sql = "UPDATE orders SET customer_id = '".$customer_id."',grand_total = '".$grand_total."',status = '".$status."' WHERE id = ".$id;
                }
                else{
                    $sql = "INSERT INTO orders (customer_id,grand_total,status) VALUES(".$customer_id.",".$grand_total.",'".$status."')";
                }
                $link->exec($sql);

            }



            $id = "";
            $customer_id = "";
            $grand_total = "";
            $status = "";

            break;
        default:

            break;
    }
    ?>

    <?php

    $recordSet = $link->query("SELECT * FROM orders")->fetchAll();

    $echo_string = "";

    foreach($recordSet as $record){
        $echo_string = $echo_string."<tr>"."<td>".$record['id']."</td>";
        $echo_string = $echo_string."<td>".$record['customer_id']."</td>";
        $echo_string = $echo_string."<td>".$record['grand_total']."</td>";
        $echo_string = $echo_string."<td>".$record['status']."</td>";


        $form_s_hidden_action = "<input type = hidden name = \"action\" value = \"S\">";
        $form_s_hidden_id = "<input type = hidden name = \"id\" value = \"".$record['id']."\">";
        $form_s_submit = "<input class='btn btn-primary' style ='margin-bottom :1em;' type = submit value = \"S\">";
        $form_s = "<form method = post action = \"#\">.$form_s_hidden_action.$form_s_hidden_id.$form_s_submit.</form>";

        $form_m_hidden_action = "<input type = hidden name = \"action\" value = \"M\">";
        $form_m_hidden_id = "<input type = hidden name = \"id\" value = \"".$record['id']."\">";
        $form_m_submit = "<input class='btn btn-primary' type = submit value = \"M\">";
        $form_m = "<form method = post action = \"#\">.$form_m_hidden_action.$form_m_hidden_id.$form_m_submit.</form>";

        $echo_string = $echo_string."<td>".$form_s.$form_m."</td>"."</tr>";
    }

    ?>
    <body>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">PMA ADMINISTRATOR</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="admin.php">Dashboard</a>
                    </li>
                    <li>
                        <a href="logout.php">Exit</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('uploads/images/arbitre.jpg')">
        <div class="container overlay">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1 style="font-family: Arial" >Delivery Management</h1>
                        <hr class="small">
                        <span class="subheading">Best ADMIN <br><strong style="font-size: 40px">EVER ! <3</strong></span>
                    </div>
                </div>
            </div>
        </div>
    </header>
        <!-- Navigation -->

    <table class="table table-striped">
        <thead>
            <tr>
            <th>N°COMMANDE</th>
            <th>N°CLIENT</th>
            <th>TOTAL</th>
            <th>STATUS</th>
            <th>ACTION</th>
        </tr>
        </thead>
        <tbody>
            <tr>
            <form method = post action = "#">
                <td><input type = number name = "id" value = "<?php echo $id?>"></td>
                <td><input type = text name = "customer_id" value = "<?php echo $customer_id?>"></td>
                <td><input type = number step="0.01" min="0" name = "grand_total"  value = "<?php echo $grand_total?>"></td>
                <td><input type = text name = "status" value = "<?php echo $status?>"></td>
                <td><input type = submit name = "action" value = "OK"></td>
            </form>

            <?php echo $echo_string ?>
        </tr>
        </tbody>
    </table>
    </body>
    </html>