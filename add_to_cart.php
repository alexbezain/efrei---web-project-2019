<?php
include_once 'CartItemClass.php';
// start session 
session_start();




// get the product id
$id_product = isset($_GET['id']) ? $_GET['id'] : "";
$quantity = isset($_GET['quantity']) ? $_GET['quantity'] : 1;
$page = isset($_GET['page']) ? $_GET['page'] : 1;
 
// make quantity a minimum of 1
$quantity=$quantity<=0 ? 1 : $quantity;
 
// add new item on array
$cart_item_to_add = new CartItem();
$cart_item_to_add->id_product = $id_product;
$cart_item_to_add->quantity = $quantity;
 
/*
 * check if the 'cart' session array was created
 * if it is NOT, create the 'cart' session array
 */
if(!isset($_SESSION['cart'])){
    $_SESSION['cart'] = array();
}
 
// check if the item is in the array, if it is, do not add
if(in_array($cart_item_to_add,$_SESSION['cart'])){
    // redirect to product list and tell the user it was added to cart
    //displayCart($_SESSION['cart']);
   /* foreach ($_SESSION['cart'] as $comparedItem) {
        if ($comparedItem -> id_product == $cart_item_to_add -> id_product) {
            $comparedItem -> quantity = $comparedItem ->quantity +1;

        }

    }*/
    header('Location: products.php?action=exists&id=' . $cart_item_to_add -> id_product . '&page=' . $page);
    //displayCart($_SESSION['cart']);
}
// else, add the item to the array
else{
    array_push($_SESSION['cart'],$cart_item_to_add);
    //echo "product added in cart";
    // redirect to product list and tell the user it was added to cart
    header('Location: products.php?action=added&page=' . $page);
}
?>