<?php
include_once 'CartItemClass.php';
// start session
session_start();
 
// connect to database
include 'config/database.php';
 
// include objects
include_once "objects/product.php";
include_once "objects/product_image.php";
//include_once "destroy_cart.php";
//include_once 'Cart.class.php'; 
//$cart = new Cart; 
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// initialize objects
$product = new Product($db);
$product_image = new ProductImage($db);
 
// set page title
$page_title="Checkout";
 
// include page header html
include 'layout_header_no_log.php';
 
if(count($_SESSION['cart'])>0){
 
    // get the product ids
    $ids = array();
    foreach($_SESSION['cart'] as $item){
        array_push($ids, $item->id_product);
    }
 
    $stmt=$product->readByIds($ids);
 
    $total=0;
    $item_count=0;
 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);

        $quantity = getQuantityInCart($_SESSION['cart'], $id);
        //$quantity=$_SESSION['cart'][$id_product]['quantity'];
        $sub_total=$price*$quantity;

 
        echo "<div class='product-id' style='display:none;'>{$id}</div>";
        //echo "<div class='product-name'>{$name}</div>";
 
        // =================
        echo "<div class='cart-row'>";
            echo "<div class='col-md-8'>";
 
                echo "<div class='product-name m-b-10px'><h4>{$name}</h4></div>";
                echo $quantity>1 ? "<div>{$quantity} items</div>" : "<div>{$quantity} item</div>";
 
            echo "</div>";
 
            echo "<div class='col-md-4'>";
                echo "<h4>&#36;" . number_format($price, 2, '.', ',') . "</h4>";
            echo "</div>";
        echo "</div>";
        // =================
 
        $item_count += $quantity;
        $total+=$sub_total;

        $_SESSION['quantity'] = $quantity;
        $_SESSION['total']=$total;
    }
 
    // echo "<div class='col-md-8'></div>";
    echo "<div class='col-md-12 text-align-center'>";
        echo "<div class='cart-row'>";
            if($item_count>1){
                echo "<h4 class='m-b-10px'>Total ({$item_count} items)</h4>";
            }else{
                echo "<h4 class='m-b-10px'>Total ({$item_count} item)</h4>";
            }
            echo "<h4>&#36;" . number_format($total, 2, '.', ',') . "</h4>";
            echo "<a id='paypal-button-container'>";
            echo "</a>";
        echo "</div>";
    echo "</div>";
 
}
 
else{
    echo "<div class='col-md-12'>";
        echo "<div class='alert alert-danger'>";
            echo "No products found in your cart!";
        echo "</div>";
    echo "</div>";
}
 
include 'layout_footer.php';

?>

<body>
    <!-- Include the PayPal JavaScript SDK -->
    <!--Id is the PAYPAL ID of the merchant -->
    <script src="https://www.paypal.com/sdk/js?client-id=ARjwOOn8D6bLU4FGTZ7tV7jhvNKR5J-6MQRNecPKxTe3h9GDPmWYwhpK1Ehw44jvqcNIsNJdILO2v4bt&currency=EUR"></script>

    <script>
        // Render the PayPal button into #paypal-button-container
        paypal.Buttons({

            // Set up the transaction
            createOrder: function(data, actions) {
                return actions.order.create({
                    purchase_units: [{
                        amount: {
                            //Add price of the cart
                            <?php echo "value: '$total' "?> 
                        }
                    }]
                });
            },

            // Finalize the transaction
            onApprove: function(data, actions) {
                return actions.order.capture().then(function(details) {
                    //Show a success message to the buyer
                    window.location = "place_order.php";

                });
            }

        }).render('#paypal-button-container');
    </script>
</body>